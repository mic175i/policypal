package com.test.policypal;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import com.test.policypal.model.Item;
import com.test.policypal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.test.policypal.model.User;
import com.test.policypal.model.User.Job;
import com.test.policypal.repository.ItemRepository;

@EnableAutoConfiguration
@SpringBootApplication
public class TestPolicyPalApplication implements CommandLineRunner {
    
    @Autowired
    private ItemRepository itemRepository;
    
    @Autowired
    private UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(TestPolicyPalApplication.class, args);
	}
	
	@Override
	public void run(String... args) {
	    List<Job> collectionOfJob = new ArrayList<>();
	    collectionOfJob.add(Job.Barbarian);
	    collectionOfJob.add(Job.Mage);
	    collectionOfJob.add(Job.Hunter);

	    /*create item*/
	    for(int i =1; i <=20; i++) {
	        int max = 4 * i;
	        for(Integer[] result : permute(max, 4, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9)) {
                itemRepository.save(new Item(i, "Item " + i, result[0], result[1], result[2], result[3]));
            }
	    }
	    
	    /*create user*/
	    for(int i = 1; i <=20; i++) {
	        User user = new User();
	        user.setName("User " + i);
	        user.setLevel(i);
	        user.setJob(getRandomObject(collectionOfJob));
            userRepository.save(user);

	        List<Item> items = itemRepository.findAllByLevelGreaterThan(i);
	        for(int x = 0;x < 5;x++) {
                user.addItem(getRandomObject(items));
            }
	        userRepository.save(user);
	    }
    }

    private <T> T getRandomObject(Collection<T> from) {
        Random rnd = new Random();
        int i = rnd.nextInt(from.size());
        return (T) from.toArray()[i];
    }

    private static List<Integer[]> permute(int max, int size, Integer... itemsPool) {
        if (size < 1) {
            return new ArrayList<>();
        }

        int itemsPoolCount = itemsPool.length;

        List<Integer[]> permutations = new ArrayList<>();
        for (int i = 0; i < Math.pow(itemsPoolCount, size); i++) {
            Integer[] permutation = (Integer[]) Array.newInstance(Integer.class, size);
            for (int j = 0; j < size; j++) {
                // Pick the appropriate item from the item pool given j and i
                int itemPoolIndex = (int) Math.floor((double) (i % (int) Math.pow(itemsPoolCount, j + 1)) / (int) Math.pow(itemsPoolCount, j));
                permutation[j] = itemsPool[itemPoolIndex];
            }
            
            Integer tmp = 0;
            for (int j = 0; j < size; j++) {
                tmp = tmp + permutation[j];
            }
            if(tmp <= max) {
                permutations.add(permutation);
            }
        }
        return permutations;
    }
}
