package com.test.policypal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "item")
public class Item {
    
    @Id
    @GeneratedValue
    private Long id;
    
    private String name;
    
    @NotNull
    @Min(1)
    @Max(20)
    private Integer level;
    
    @Min(0)
    @NotNull
    private Integer strength;
    
    @Min(0)
    @NotNull
    private Integer dexterity;
    
    @Min(0)
    @NotNull
    private Integer intelligence;
    
    @Min(0)
    @NotNull
    private Integer vitality;
    
//    @ManyToOne
//    private User user;
    
    public Item() {}
    
    public Item(int level, String name, int strength, int dexterity, int intelligence, int vitality) {
        this.level = level;
        this.name = name;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getLevel() {
        return this.level;
    }
    
    public void setLevel(int level) {
        this.level = level;
    }
    
    public int getStrength() {
        return this.strength;
    }
    
    public void setStrength(int strength) {
        this.strength = strength;
    }
    
    public int getDexterity() {
        return this.dexterity;
    }
    
    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }
    
    public int getIntelligence() {
        return this.intelligence;
    }
    
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
    
    public int getVitality() {
        return this.vitality;
    }
    
    public void setVitality(int vitality) {
        this.vitality = vitality;
    }
    
//    public User getUser() {
//        return this.user;
//    }
//    
//    public void setUser(User user) {
//        this.user = user;
//    }
}
