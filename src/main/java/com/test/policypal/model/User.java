package com.test.policypal.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user")
public class User {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @Min(1)
    @Max(20)
    @NotNull
    private int level;
    
    private String name;
 
    @NotNull
    @Enumerated(EnumType.STRING)
    private Job job;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "user_id")
    private List<Item> items = new ArrayList<>();
    
    public enum Job {
        Barbarian,
        Mage,
        Hunter
    }
    
    public User() {}
    
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public int getLevel() {
        return this.level;
    }
    
    public void setLevel(int level) {
        this.level = level;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Job getJob() {
        return this.job;
    }
    
    public void setJob(Job job) {
        this.job = job;
    }
    
    public List<Item> getItems() {
        return this.items;
    }
    
    public void addItem(Item item) {
        this.items.add(item);
    }
}
