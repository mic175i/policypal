package com.test.policypal.repository;

import com.test.policypal.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    
    @Query("SELECT u, ((u.level * 3)+SUM(i.strength)) + ((u.level * 3)+SUM(i.vitality)) as total " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Barbarian' " +
            "GROUP BY u.name ORDER BY total DESC")
    List<User> findBySumAttributeJobBarbarian(@Param("level") int level);

    @Query("SELECT u,((u.level * 3)+SUM(i.strength)) + ((u.level * 3)+SUM(i.vitality)) as total " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Barbarian' " +
            "GROUP BY u.name ORDER BY total DESC")
    List<User> findBySumAttributeJobBarbarianWithLimit(@Param("level") int level, Pageable pageable);

    @Query("SELECT u, ((u.level * 3)+SUM(i.intelligence)) + ((u.level * 3)+SUM(i.vitality)) as total " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Mage' " +
            "GROUP BY u.name ORDER BY total DESC")
    List<User> findBySumAttributeJobMage(@Param("level") int level);

    @Query("SELECT u, ((u.level * 3)+SUM(i.intelligence)) + ((u.level * 3)+SUM(i.vitality)) as total " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Mage' " +
            "GROUP BY u.name ORDER BY total DESC")
    List<User> findBySumAttributeJobMageWithLimit(@Param("level") int level, Pageable pageable);

    @Query("SELECT u, ((u.level * 3)+SUM(i.dexterity)) + ((u.level * 3)+SUM(i.vitality)) as total " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Hunter' " +
            "GROUP BY u.name ORDER BY total DESC")
    List<User> findBySumAttributeJobHunter(@Param("level") int level);

    @Query("SELECT u,((u.level * 3)+SUM(i.dexterity)) + ((u.level * 3)+SUM(i.vitality)) as total " +
            "from User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Hunter' " +
            "GROUP BY u.name ORDER BY total DESC")
    List<User> findBySumAttributeJobHunterWithLimit(@Param("level") int level, Pageable pageable);

    @Query("SELECT u, i.strength * 3 , i.dexterity * 2, i.intelligence * 1, i.vitality * 3 " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Barbarian' " +
            "ORDER BY i.strength DESC, i.vitality DESC")
    List<User> findByAttributeJobBarbarian(@Param("level") int level);

    @Query("SELECT u, i.strength * 3 , i.dexterity * 2, i.intelligence * 1, i.vitality * 3 " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Barbarian' " +
            "ORDER BY i.strength DESC, i.vitality DESC")
    List<User> findByAttributeJobBarbarianWithLimit(@Param("level") int level, Pageable pageable);

    @Query("SELECT u, i.strength * 1 , i.dexterity * 2, i.intelligence * 3, i.vitality * 3 " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Mage' " +
            "ORDER BY i.intelligence DESC, i.vitality DESC")
    List<User> findByAttributeJobMage(@Param("level") int level);

    @Query("SELECT u, i.strength * 1 , i.dexterity * 2, i.intelligence * 3, i.vitality * 3 " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Mage' " +
            "ORDER BY i.intelligence DESC, i.vitality DESC")
    List<User> findByAttributeJobMageWithLimit(@Param("level") int level, Pageable pageable);

    @Query("SELECT u, i.strength * 2 , i.dexterity * 3, i.intelligence * 1, i.vitality * 3 " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Hunter' " +
            "ORDER BY i.dexterity DESC, i.vitality DESC")
    List<User> findByAttributeJobHunter(@Param("level") int level);

    @Query("SELECT u, i.strength * 2 , i.dexterity * 3, i.intelligence * 1, i.vitality * 3 " +
            "FROM User u JOIN u.items i " +
            "WHERE u.level >= :level AND u.job = 'Hunter' " +
            "ORDER BY i.dexterity DESC, i.vitality DESC")
    List<User> findByAttributeJobHunterWithLimit(@Param("level") int level, Pageable pageable);

    List<User> findByLevel(int level);
}
