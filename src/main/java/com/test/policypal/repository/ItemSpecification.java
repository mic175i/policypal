package com.test.policypal.repository;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.test.policypal.model.Item;
import org.springframework.data.jpa.domain.Specification;

public class ItemSpecification {
    
    public static Specification<Item> findit(int level, Integer strength, Integer dexterity,
                                             Integer intelligence, Integer vitality, String... sorts) {
    return new Specification<Item>() {
      public Predicate toPredicate(Root<Item> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
          final Collection<Predicate> predicates = new ArrayList<>();
          
          Predicate levelP = builder.greaterThanOrEqualTo(root.get("level"), level);
          predicates.add(levelP);
          
          if (strength != null) {
              final Predicate strengthP = builder.greaterThanOrEqualTo(root.get("strength"), strength);
              predicates.add(strengthP);
          }
          if (dexterity != null) {
              final Predicate dexterityP = builder.greaterThanOrEqualTo(root.get("dexterity"), dexterity);
              predicates.add(dexterityP);
          }
          if (intelligence != null) {
              final Predicate intelligenceP = builder.greaterThanOrEqualTo(root.get("intelligence"), intelligence);
              predicates.add(intelligenceP);
          }
          if (vitality != null) {
              final Predicate vitalityP = builder.greaterThanOrEqualTo(root.get("vitality"), vitality);
              predicates.add(vitalityP);
          }
          
          //*order by*/
          if(sorts != null) {
              Order[] orders = new Order[sorts.length];
              for(int i=0;i<sorts.length;i++) {
                  orders[i] = builder.desc(root.get(sorts[i]));
              }
              query.orderBy(orders);
          }
          
          return builder.and(predicates.toArray(new Predicate[predicates.size()]));
      }
    };
  }
}
