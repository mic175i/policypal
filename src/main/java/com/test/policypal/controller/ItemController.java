package com.test.policypal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.policypal.model.Item;
import com.test.policypal.model.User.Job;
import com.test.policypal.repository.ItemRepository;
import com.test.policypal.repository.ItemSpecification;

@RestController
public class ItemController {
    
    @Autowired
    private ItemRepository itemRepository;
    
    @GetMapping("/items")
    public List<Item> getItem(
        @RequestParam(value="level") int level, 
        @RequestParam(value="job") Job job,
        @RequestParam(value="limit", required=false) Integer limit,
        @RequestParam(value="strength", required=false) Integer strength,
        @RequestParam(value="dexterity", required=false) Integer dexterity,
        @RequestParam(value="intelligence", required=false) Integer intelligence,
        @RequestParam(value="vitality", required=false) Integer vitality
        ) {
            
            String[] orderby = null;
            switch(job) {
                case Barbarian :
                orderby = new String[]{"strength", "vitality"};
                break;
                case Mage :
                orderby = new String[]{"intelligence", "vitality"};
                break;
                case Hunter :
                orderby = new String[]{"dexterity", "vitality"};
                break;
            }
            if(limit != null) {
                Pageable pageRequest = new PageRequest(0, limit);
                return itemRepository.findAll(ItemSpecification.findit(level, strength, dexterity,
                        intelligence, vitality, orderby), pageRequest).getContent();
            }
            return itemRepository.findAll(ItemSpecification.findit(level, strength, dexterity,
                    intelligence, vitality, orderby));
    }
}
