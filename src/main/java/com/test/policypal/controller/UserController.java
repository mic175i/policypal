package com.test.policypal.controller;

import com.test.policypal.repository.UserRepository;
import com.test.policypal.model.User;
import com.test.policypal.model.User.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class UserController {
    
    @Autowired
    private UserRepository userRepository;
    
    @RequestMapping("/users/orderByJobAttribute")
    public Collection<User> orderByJob(
        @RequestParam(value="level") Integer level,
        @RequestParam(value="job") Job job,
        @RequestParam(value="limit", required=false) Integer limit) {

            switch(job) {
                case Barbarian :
                    if(limit != null) {
                        Pageable pagerequest = new PageRequest(0, limit);
                        return userRepository.findByAttributeJobBarbarianWithLimit(level, pagerequest);
                    }
                    return userRepository.findByAttributeJobBarbarian(level);
                case Mage :
                    if(limit != null) {
                        Pageable pagerequest = new PageRequest(0, limit);
                        return userRepository.findByAttributeJobMageWithLimit(level, pagerequest);
                    }
                    return userRepository.findByAttributeJobMage(level);
                case Hunter :
                    if(limit != null) {
                        Pageable pagerequest = new PageRequest(0, limit);
                        return userRepository.findByAttributeJobHunterWithLimit(level, pagerequest);
                    }
                    return userRepository.findByAttributeJobHunter(level);
            }
            return userRepository.findByLevel(level);
    }
    
    @RequestMapping("/users/orderBySumOfJobAttribute")
    public Collection<User> orderBySumOfJobAttribute(
        @RequestParam(value="level") Integer level,
        @RequestParam(value="job") Job job,
        @RequestParam(value="limit", required = false) Integer limit) {

        switch(job) {
            case Barbarian :
                if(limit != null) {
                    return userRepository.findBySumAttributeJobBarbarianWithLimit(level, new PageRequest(0,limit));
                }
                return userRepository.findBySumAttributeJobBarbarian(level);
            case Mage :
                if(limit != null) {
                    return userRepository.findBySumAttributeJobMageWithLimit(level, new PageRequest(0,limit));
                }
                return userRepository.findBySumAttributeJobMage(level);
            default:
                if(limit != null) {
                    return userRepository.findBySumAttributeJobHunterWithLimit(level, new PageRequest(0,limit));
                }
                return userRepository.findBySumAttributeJobHunter(level);
        }
    }
}
